package com.example.tfc02_messager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02_messager.data.DAOChannelSingleton
import com.example.tfc02_messager.model.ChatMessage
import com.example.tfc02_messager.ui.list.adapters.MessageAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.time.LocalDateTime

class MainActivity : AppCompatActivity() {
    private lateinit var messages: ArrayList<ChatMessage>
    private lateinit var newMessages: ArrayList<ChatMessage>
    private lateinit var etxtInputMessage: EditText
    private lateinit var rvChatList: RecyclerView
    private lateinit var chatMessageAdapter: MessageAdapter
    private var channelId: Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.etxtInputMessage = findViewById(R.id.etxtInputMessage)
        this.rvChatList = findViewById(R.id.rvChatList)
        this.channelId = intent.getLongExtra("channelId", -1)
        val chat = DAOChannelSingleton.getChannelById(channelId)
        if (chat != null) {
            this.messages = chat.getMessages()
        }
        this.newMessages = ArrayList()
    }

    override fun onBackPressed() {
        val output = Intent()
        output.putExtra("channelId", this.channelId)
        setResult(RESULT_OK, output)
        super.onBackPressed()
    }

    fun onClickInsertMessage(view: View) {
        val text = this.etxtInputMessage.text.toString()
        if(text.trim().isNotEmpty()) {
            val channel = DAOChannelSingleton.getChannelById(channelId)
            this.etxtInputMessage.text.clear()
            if (channel != null) {
                val author = channel.getRandomUser()
                val m = ChatMessage(author.id, text, LocalDateTime.now())
                channel.addMessage(m)
                this.rvChatList.adapter?.notifyItemInserted(channel.getMessages().size-1)
                this.rvChatList.smoothScrollToPosition(channel.getMessages().size-1)
            }
        }
    }

}