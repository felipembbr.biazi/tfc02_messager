package com.example.tfc02_messager.ui.list.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02_messager.R
import com.example.tfc02_messager.model.ChatChannel
import com.example.tfc02_messager.ui.list.viewholders.ChannelViewHolder

class ChannelAdapter(
    private val chats: ArrayList<ChatChannel>
): RecyclerView.Adapter<ChannelViewHolder>() {
    private var listener: OnClickedChatListener? = null

    fun interface OnClickedChatListener {
        fun onClickChat(channel: ChatChannel)
    }
    // implementacao do adapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            R.layout.itemview_chat,
            parent,
            false
        )
        return ChannelViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        holder.bind(this.chats[position])
    }

    override fun getItemCount(): Int {
        return this.chats.size
    }

    fun setOnClickedChatListener(listener: OnClickedChatListener?) {
        this.listener = listener
    }

    fun getOnClickedChatListener(): OnClickedChatListener? {
        return this.listener
    }
}