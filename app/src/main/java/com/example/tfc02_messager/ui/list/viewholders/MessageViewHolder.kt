package com.example.tfc02_messager.ui.list.viewholders

import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02_messager.R
import com.example.tfc02_messager.data.DAOUsersSingleton
import com.example.tfc02_messager.model.ChatMessage
import com.example.tfc02_messager.ui.list.adapters.MessageAdapter

class MessageViewHolder(
    itemView: View,
    private val adapter: MessageAdapter
): RecyclerView.ViewHolder(itemView) {
    private val txtOwner: TextView
    private val txtText: TextView
    private val txtTimestamp: TextView
    private lateinit var chatMessageCurrent: ChatMessage
    init {
        this.txtOwner = itemView.findViewById(R.id.txtOwner)
        this.txtText = itemView.findViewById(R.id.txtText)
        this.txtTimestamp = itemView.findViewById(R.id.txtTimestamp)
        itemView.setOnClickListener {
            adapter.getOnClickBallonListener()?.onClickBallon(this.chatMessageCurrent)
        }
    }
    fun bind(chatMessage: ChatMessage) {
        this.chatMessageCurrent = chatMessage
        this.txtOwner.text = this.chatMessageCurrent.owner.toString()
        this.txtText.text = this.chatMessageCurrent.text
        this.txtTimestamp.text = this.chatMessageCurrent.timestamp.toString()
    }
}
