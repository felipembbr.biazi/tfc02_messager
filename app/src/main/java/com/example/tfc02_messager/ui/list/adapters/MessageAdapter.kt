package com.example.tfc02_messager.ui.list.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02_messager.R
import com.example.tfc02_messager.model.ChatMessage
import com.example.tfc02_messager.ui.list.viewholders.MessageViewHolder

class MessageAdapter(
    private val messages: ArrayList<ChatMessage>
): RecyclerView.Adapter<MessageViewHolder>() {
    private var listener: OnClickBallonListener? = null
    companion object {
        private val TYPE_SEND = 0
        private val TYPE_RCVD = 1
    }
    fun interface OnClickBallonListener {
        fun onClickBallon(chatMessage: ChatMessage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val layoutRes = if(viewType == TYPE_SEND)
            R.layout.itemview_balao_enviado
        else R.layout.itemview_balao_recebido
        val itemView = inflater.inflate(layoutRes, parent, false)
        return MessageViewHolder(itemView, this)
    }
    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.bind(this.messages[position])
    }
    override fun getItemCount(): Int {
        return this.messages.size
    }

    /*override fun getItemViewType(position: Int): Int {
        return if(this.messages[position].isReceived) TYPE_RCVD else TYPE_SEND
    }*/

    // ------------- Implementação adicional do adapter

    fun setOnClickBallonListener(listener: OnClickBallonListener?) {
        this.listener = listener
    }

    fun getOnClickBallonListener(): OnClickBallonListener? {
        return this.listener
    }
}