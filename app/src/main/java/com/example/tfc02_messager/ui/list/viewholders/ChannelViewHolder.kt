package com.example.tfc02_messager.ui.list.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02_messager.R
import com.example.tfc02_messager.model.ChatChannel
import com.example.tfc02_messager.model.ChatMessage
import com.example.tfc02_messager.ui.list.adapters.ChannelAdapter

class ChannelViewHolder(
    itemView: View,
    private val adapter: ChannelAdapter
): RecyclerView.ViewHolder(itemView) {
    private var txtChatTitle: TextView =
        itemView.findViewById(R.id.txtChatTitle)
    private var txtLastMessage: TextView =
        itemView.findViewById(R.id.txtLastMessage)
    private lateinit var currentChat: ChatChannel
    init {
        itemView.setOnClickListener {
            this.adapter.getOnClickedChatListener()?.onClickChat(this.currentChat)
        }
    }
    fun bind(channel: ChatChannel) {
        this.currentChat = channel
        this.txtChatTitle.text = this.currentChat.name
        this.txtLastMessage.text = this.currentChat.getLastMessage()
    }
}