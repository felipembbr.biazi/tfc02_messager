package com.example.tfc02_messager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc02_messager.data.DAOChannelSingleton
import com.example.tfc02_messager.ui.list.adapters.ChannelAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ChatListActivity : AppCompatActivity() {
    private lateinit var rvChatIndex: RecyclerView
    private lateinit var btnAddChannel: FloatingActionButton
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_list)
        this.rvChatIndex = findViewById(R.id.rvChatIndex)
        this.btnAddChannel = findViewById(R.id.btnAddChannel)
        this.rvChatIndex.layoutManager = LinearLayoutManager(this)
        val adapter = ChannelAdapter(DAOChannelSingleton.getChannels())
        this.rvChatIndex.adapter = adapter
        val resultLauncher = registerForActivityResult(
            ActivityResultContracts.StartIntentSenderForResult()
        ) { result ->
            if(result.resultCode == RESULT_OK && result.data != null) {
                val channelId = result.data!!.getLongExtra("channelId", -1)
                val channelPosition = DAOChannelSingleton.getChannelPositionById(channelId)
                adapter.notifyItemChanged(channelPosition)
                adapter.notifyDataSetChanged()
            }
          }
        adapter.setOnClickedChatListener { channel ->
            val openChannelIntent =
                Intent(this, MainActivity::class.java)
            openChannelIntent.putExtra("channelId", channel.id)
            resultLauncher.launch(openChannelIntent)
        }
    }

    fun onClickInsert(view: View) {
        DAOChannelSingleton.add()
        this.rvChatIndex.adapter?.notifyItemInserted(0)
        this.rvChatIndex.smoothScrollToPosition(0)
    }
}